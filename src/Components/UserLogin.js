import React, {useEffect, useState} from 'react';
import {Button, Modal} from 'react-bootstrap';
import './UserRegistration.css';
const LoginPage = ({showLogin, handleLoginClose}) => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [passwordError, setpasswordError] = useState("");
  const [emailErrorLogin, setemailErrorLogin] = useState("");
  const [name,setName]=useState("");
  const handleValidation = (event) => {
    let formIsValid = true;
    if (!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailErrorLogin("Email Not Valid");
      return false;
    } else {
      setemailErrorLogin("");
      formIsValid = true;
    }
    return formIsValid;
  };
  let handleLoginSubmit = async (e) => {
    e.preventDefault();
    if(email===""){
      setemailErrorLogin("email can not be empty");
      return;
    }else{
      setemailErrorLogin("");
    }
    if(password===""){
      setpasswordError("password can not be empty");
      return;
    }else{
      setpasswordError("");
    }
    if(!handleValidation()){
      return;
    }
    let registered_user_id;
    let registered_name;
    for (let i = 0; i < users.length; i++) {
      if(users[i].email === email && users[i].user_password !== password){
        setpasswordError("password doesn't match!");
        return;
      }
      if(users[i].email === email && users[i].user_password === password){
        registered_user_id = users[i].user_id;
        registered_name=users[i].fullname;
        setName(registered_name);
      }
    }
    try {
      let res = await fetch('https://kalavithi-service-aquila-dev.herokuapp.com/api/login', {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
           'userId': registered_user_id,
        }),
      });
      if (res.status === 200) {
        setEmail("");
        setPassword("");
        console.log("User logged in successfully");
      } else {
        console.log("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };
  const [users, setUsers] = useState([]);
    const getUsersRequest = async () => {
    const response = await fetch('https://kalavithi-service-aquila-dev.herokuapp.com/api/users');
    const responseJson = await response.json();
    console.log(responseJson);
    setUsers(responseJson);
  };
  useEffect(() => {
    getUsersRequest();
  }, []);
  return <>
      <Modal show={showLogin} onHide={handleLoginClose} dialogClassName="my-modal">
        <Modal.Header closeButton className="modal-header">
          <Modal.Title className="modal-title">User Login</Modal.Title>
         </Modal.Header>
        <Modal.Body className="modal-body">
         <form onSubmit={handleLoginSubmit}>
              <div id="formdiv">
               <label className="required" >Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text error-text">
                  {emailErrorLogin}
                </small>
              </div>
              <div className="mt-4">
                <label className="required">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text error-text">
                  {passwordError}
                </small>
              </div>
              <Button type="submit" variant="primary" className="modal-btn mt-5" size="lg">
                Login
              </Button>
              <p className="login-text">Not having account? <a href='#'>Register</a> here</p>
            </form>
          </Modal.Body>
        </Modal>
  </>;
};
export default LoginPage;