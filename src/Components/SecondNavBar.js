import React, { useState } from "react";
import "./SecondNavBar.css";
import { GiHamburgerMenu } from "react-icons/gi";
import Logo from "./Logo";
import UserRegistration from './UserRegistration';
import UserLogin from './UserLogin';
import 'font-awesome/css/font-awesome.min.css';
import DropDownPage from './DropDownPage';


const SecondNavBar = () => {


  const [show, setShow] = useState(false);
  const handleShow = () => {
    setShow(true);
  }
  const handleClose = () => {
     setShow(false);
  }

  const [showLogin, setLoginShow] = useState(false);
  const handleLoginShow = () => {
    setLoginShow(true);
  }
  const handleLoginClose = () => {
     setLoginShow(false);
  }

  const [showMediaIcons, setShowMediaIcons] = useState(false);
  return (
    <>
      <nav className="main-nav">

        <div className="logo">
          <h2>
            <Logo/>
          </h2>
        </div>

        <div
          className={
            showMediaIcons ? "menu-link mobile-menu-link" : "menu-link"
          }>
            <ul>
                <li>
                  <div class="search">
                        <input type="text" name="search" id="search" placeholder="&#x1F50E; Search"/>
                  </div>
              </li>
            </ul>
        </div>


        <div className="social-media">
          <ul className="social-media-desktop">

          <ul>
            <li>
             <DropDownPage />
            </li>
          </ul>

          {/* <ul>
            <li>
              <button className="btn" id="btn_1" onClick={handleShow}>Register</button>
              <UserRegistration show={show} handleClose={handleClose}/>
            </li>
          </ul> */}

          {/* <ul>
            <li>
               <button className="btn" id="btn_1" onClick={handleLoginShow}>Login</button>
               <UserLogin showLogin={showLogin} handleLoginClose={handleLoginClose}/>
            </li>
          </ul> */}
          
          {/* hamburget menu start  */}
          <div className="hamburger-menu">
            <a href="#" onClick={() => setShowMediaIcons(!showMediaIcons)}>
              <GiHamburgerMenu />
            </a>
          </div>
          </ul>
        </div>
      </nav>
    </>
  );
};

export default SecondNavBar;