import React, {useEffect, useState} from 'react';
import {Modal, Button} from 'react-bootstrap';
import './UserRegistration.css';
const UserRegistration = ({show, handleClose, openLogin}) => {
  const [fullname, setFullname] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [passwordError, setpasswordError] = useState("");
  const [emailError, setemailError] = useState("");
  const [fullnameError, setfullnameError] = useState("");
  const handleValidation = (event) => {
    let formIsValid = true;
    if (!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      formIsValid = false;
      setemailError("Email Not Valid");
      return false;
    } else {
      setemailError("");
      formIsValid = true;
    }
    if (!password.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$/)) {
      formIsValid = false;
      setpasswordError(
        "Must contain at least one number and one uppercase and lowercase letter and one special character, length must be min 8 Chracters and Max 15 Chracters"
      );
      return false;
    } else {
      setpasswordError("");
      formIsValid = true;
    }
    return formIsValid;
  };
  let handleSubmit = async (e) => {
    e.preventDefault();
    if(fullname===""){
      setfullnameError("* name can not be empty!");
      return;
    }else{
      setfullnameError("");
    }
    if(email===""){
      setemailError("* email can not be empty!");
      return;
    }else{
      setemailError("");
    }
    if(password===""){
      setpasswordError("* password can not be empty!");
      return;
    }else{
      setpasswordError("");
    }
    if(!handleValidation()){
      return;
    }
    for (let i = 0; i < users.length; i++) {
      if (users[i].email === email) {
        setemailError("Email alreday exists");
        return;
      }
    }
    //const url = 'http://localhost:8090/api/users';
    //https://kalavithi-service-aquila-dev.herokuapp.com/api/users
    try {
      let res = await fetch('https://kalavithi-service-aquila-dev.herokuapp.com/api/users', { 
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
           'fullname': fullname,
           'email': email,
           'user_password': password
        }),
      });
      if (res.status === 200) {
        setFullname("");
        setEmail("");
        setPassword("");
        console.log("User created successfully");
        alert("User created successfully");
      } else {
        console.log("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };
  const [users, setUsers] = useState([]);
  const getUsersRequest = async () => {
    //const url = 'http://localhost:8090/api/users';
  //https://kalavithi-service-aquila-dev.herokuapp.com/api/users
    const response = await fetch('https://kalavithi-service-aquila-dev.herokuapp.com/api/users');
    const responseJson = await response.json();
    console.log(responseJson);
    setUsers(responseJson);
  };
  useEffect(() => {
    getUsersRequest();
  }, []);
  return <>
    <Modal show={show} onHide={handleClose}  dialogClassName="my-modal">
        <Modal.Header closeButton className="modal-header" >
          <Modal.Title className="modal-title">Register New User</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body">
         <form id="registrationform" onSubmit={handleSubmit}>
             <div className="form-group">
               <label className="required">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="NameInput"
                  name="NameInput"
                  placeholder="Enter name"
                  value={fullname}
                  onChange={(event) => setFullname(event.target.value)}
                />
                <small id="fullnameHelp" className="text-danger form-text error-text">
                  {fullnameError}
                </small>
              </div>
              <div className="form-group mt-4">
               <label className="required">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="EmailInput"
                  name="EmailInput"
                  aria-describedby="emailHelp"
                  placeholder="Enter email"
                  value={email}
                  onChange={(event) => setEmail(event.target.value)}
                />
                <small id="emailHelp" className="text-danger form-text error-text">
                  {emailError}
                </small>
              </div>
              <div className="form-group mt-4">
                <label className="required">Password</label>
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  value={password}
                  onChange={(event) => setPassword(event.target.value)}
                />
                <small id="passworderror" className="text-danger form-text error-text">
                  {passwordError}
                </small>
              </div>
              <Button type="submit" variant="primary" className="modal-btn mt-5" size="lg">
                Register
              </Button>
              <p className="login-text">Already have account? <a href='#'>Login</a> here</p>
            </form>
        </Modal.Body>
      </Modal>
  </>;
};
export default UserRegistration;





